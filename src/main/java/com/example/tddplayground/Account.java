package com.example.tddplayground;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class Account {

    String name;
    BigDecimal amount;
}

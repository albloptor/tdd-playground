package com.example.tddplayground;

import org.junit.ClassRule;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

@SpringBootTest
class TddPlaygroundApplicationTests {

    @ClassRule
    public PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(DockerImageName.parse("postgres:13.1-alpine"))
            .withDatabaseName("shop")
            .withUsername("test")
            .withPassword("test")
            .withExposedPorts(5432);

    @Test
    void contextLoads() {
        System.out.println(postgres.getDatabaseName());
    }

}

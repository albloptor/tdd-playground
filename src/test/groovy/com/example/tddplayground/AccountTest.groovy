package com.example.tddplayground

import spock.lang.Specification

class AccountTest extends Specification {

    def "test"() {
        when:
        def accountId = new Account("Main account", BigDecimal.ZERO)
        accountId = new Account("Main account", BigDecimal.ZERO)

        then:
        accountId.name == "Main account"
        accountId.amount == BigDecimal.ZERO
    }
}
